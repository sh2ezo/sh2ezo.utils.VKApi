﻿using System.Net.Http;
using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using sh2ezo.utils.VKApi.Models;

namespace sh2ezo.utils.VKApi
{
    public class AccessTokenSource
    {
        static readonly Regex rxAuthLink = new Regex("action=\"(https:\\/\\/login\\.vk\\.com[^\"]+)");
        static readonly Regex rxRegisterLink = new Regex("<a[^>]+href=\"\\/join\">");
        static readonly Regex rxAccessToken = new Regex("access_token=([^&]+)");
        static readonly Regex rxUserId = new Regex("user_id=([^&]+)");
        static readonly Regex rxGrantAccessLink = new Regex("action=\"([^\"]+)");
        static readonly Regex rxExpiresIn = new Regex("expires_in=([^&]+)");
        static readonly Regex rxErrorDescription = new Regex("error_description=([^&]+)");
        static readonly Regex rxGroupAccessToken = new Regex("access_token_[^=]+=([^&]+)");

        HttpClient _client;
        RequestBuilder _builder;
        UserData _user;

        public AccessTokenSource(HttpClient client, RequestBuilder builder, UserData user)
        {
            _client = client;
            _builder = builder;
            _user = user;
        }

        public async Task<Result<bool>> LoginAsync()
        {
            try
            {
                var authPageReq = _builder.Build("GET", "https://m.vk.com/");
                var authPageResp = await _client.SendAsync(authPageReq).ConfigureAwait(false);
                authPageResp.EnsureSuccessStatusCode();
                var authPageContent = await authPageResp.Content.ReadAsStringAsync().ConfigureAwait(false);
                var authLink = rxAuthLink.Match(authPageContent).Groups[1].Value;
                var authReq = _builder.Build("POST", authLink);
                authReq.Content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "email", _user.Username },
                    { "pass", _user.Password }
                });
                var authResp = await _client.SendAsync(authReq).ConfigureAwait(false);
                authResp.EnsureSuccessStatusCode();
                var authRespContent = await authResp.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (rxRegisterLink.IsMatch(authRespContent))
                {
                    throw new WrongCredentialsException();
                }

                return Result<bool>.Success(true);
            }
            catch (Exception ex)
            {
                return Result<bool>.Fail(ex, false);
            }
        }

        public async Task<Result<AppAuthData>> UserAuthAsync(AccessRights rights)
        {
            try
            {
                var oauthLink = $"https://oauth.vk.com/authorize?client_id={_user.AppID}&redirect_uri=https://oauth.vk.com/blank.html&display=mobile&scope={rights:D}&response_type=token&v={Constants.Version}&state=123&revoke=0";
                var oauthReq = _builder.Build("GET", oauthLink);
                var oauthResp = await _client.SendAsync(oauthReq).ConfigureAwait(false);
                oauthResp.EnsureSuccessStatusCode();
                var redirectUri = oauthResp.RequestMessage.RequestUri.ToString();

                if (!redirectUri.Contains("access_token") && !redirectUri.Contains("error"))
                {
                    var oauthRespContent = await oauthResp.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var grantAccessLink = rxGrantAccessLink.Match(oauthRespContent).Groups[1].Value;

                    if (string.IsNullOrEmpty(grantAccessLink))
                    {
                        throw new WrongOAuthResponseException(oauthRespContent);
                    }

                    var grantAccessReq = _builder.Build("GET", grantAccessLink);
                    oauthResp = await _client.SendAsync(grantAccessReq).ConfigureAwait(false);
                    oauthResp.EnsureSuccessStatusCode();
                    redirectUri = oauthResp.RequestMessage.RequestUri.ToString();
                }
                else if (rxErrorDescription.IsMatch(redirectUri))
                {
                    throw new Exception(rxErrorDescription.Match(redirectUri).Groups[1].Value);
                }

                var expiresIn = int.Parse(rxExpiresIn.Match(redirectUri).Groups[1].Value);

                return Result<AppAuthData>.Success(new AppAuthData
                {
                    AccessToken = rxAccessToken.Match(redirectUri).Groups[1].Value,
                    Rights = rights,
                    SubjectID = long.Parse(rxUserId.Match(redirectUri).Groups[1].Value),
                    TokenExpireTime = DateTime.Now.AddSeconds(expiresIn)
                });
            }
            catch (Exception ex)
            {
                return Result<AppAuthData>.Fail(ex, default(AppAuthData));
            }
        }

        public async Task<Result<AppAuthData>> GroupAuthAsync(ulong groupId, AccessRights rights)
        {
            try
            {
                var oauthLink = $"https://oauth.vk.com/authorize?client_id={_user.AppID}&group_ids={groupId}&redirect_uri=https://oauth.vk.com/blank.html&display=mobile&scope={rights:D}&response_type=token&v={Constants.Version}&state=123&revoke=0";
                var oauthReq = _builder.Build("GET", oauthLink);
                var oauthResp = await _client.SendAsync(oauthReq).ConfigureAwait(false);
                oauthResp.EnsureSuccessStatusCode();
                var redirectUri = oauthResp.RequestMessage.RequestUri.ToString();

                if (!redirectUri.Contains("access_token") && !redirectUri.Contains("error"))
                {
                    var oauthRespContent = await oauthResp.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var grantAccessLink = rxGrantAccessLink.Match(oauthRespContent).Groups[1].Value;

                    if (string.IsNullOrEmpty(grantAccessLink))
                    {
                        throw new WrongOAuthResponseException(oauthRespContent);
                    }

                    var grantAccessReq = _builder.Build("GET", grantAccessLink);
                    oauthResp = await _client.SendAsync(grantAccessReq).ConfigureAwait(false);
                    oauthResp.EnsureSuccessStatusCode();
                    redirectUri = oauthResp.RequestMessage.RequestUri.ToString();
                }
                else if (rxErrorDescription.IsMatch(redirectUri))
                {
                    throw new Exception(rxErrorDescription.Match(redirectUri).Groups[1].Value);
                }

                var expiresIn = int.Parse(rxExpiresIn.Match(redirectUri).Groups[1].Value);
                return Result<AppAuthData>.Success(new AppAuthData
                {
                    AccessToken = rxGroupAccessToken.Match(redirectUri).Groups[1].Value,
                    Rights = rights,
                    TokenExpireTime = DateTime.Now.AddSeconds(expiresIn),
                    SubjectID = -((long)groupId)
                });
            }
            catch (Exception ex)
            {
                return Result<AppAuthData>.Fail(ex, default(AppAuthData));
            }
        }
    }
}
