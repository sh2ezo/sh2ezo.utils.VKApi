﻿using System;
using System.Collections.Generic;

namespace sh2ezo.utils.VKApi
{
    public struct UserData
    {
        public string Username;
        public string Password;
        public long UserID;
        public long AppID;
    }
}
