﻿using System.Net.Http;

namespace sh2ezo.utils.VKApi
{
    public class RequestBuilder
    {
        private string _userAgent;

        public HttpRequestMessage Build(string method, string url)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), url);
            request.Headers.Add("User-Agent", _userAgent);
            return request;
        }

        public RequestBuilder(string userAgent)
        {
            _userAgent = userAgent;
        }
    }
}
