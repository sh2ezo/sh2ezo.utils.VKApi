﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace sh2ezo.utils.VKApi
{
    public class AuthDataStorage
    {
        public delegate bool TokenExpiredHandler(AuthDataStorage storage, long subjectId);
        public event TokenExpiredHandler TokenExpired;

        private Dictionary<long, AppAuthData> storage = new Dictionary<long, AppAuthData>();
        private Dictionary<long, Timer> timers = new Dictionary<long, Timer>();
        private object syncRoot = new object();

        public string GetAccessToken(long subjectId)
        {
            lock(syncRoot)
            {
                if(storage.TryGetValue(subjectId, out var authData))
                {
                    return authData.AccessToken;
                }
                else
                {
                    throw new KeyNotFoundException("No such subject!");
                }
            }
        }

        public void AddOrUpdate(long subjectId, AppAuthData authData)
        {
            lock(syncRoot)
            {
                storage[subjectId] = authData;
                var timer = new Timer(o => TokenExpired?.Invoke(this, (long)o), subjectId, (authData.TokenExpireTime - DateTime.Now), new TimeSpan(-1));
                timers[subjectId] = timer;
            }
        }
    }
}
