﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi
{
    public class ShortLinkInfo
    {
        [JsonProperty("short_url")]
        public string ShortUrl { get; set; }
        [JsonProperty("access_key")]
        public string AccessKey { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
