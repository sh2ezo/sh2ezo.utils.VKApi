﻿using sh2ezo.utils.VKApi.Models;
using System;

namespace sh2ezo.utils.VKApi
{
    public class TokenExpiredException : Exception
    {
        public TokenExpiredException() : 
            base("Access token expired!")
        {

        }
    }
    public class WrongCredentialsException : Exception
    {
        public WrongCredentialsException() :
            base("Wrong user credentials!")
        {

        }
    }
    public class WrongOAuthResponseException : Exception
    {
        public string ResponseContent { get; set; }

        public WrongOAuthResponseException(string content) : 
            base("Wrong oauth response!")
        {
            ResponseContent = content;
        }
    }
    public class NoSuchRightsExceptions : Exception
    {
        public NoSuchRightsExceptions(AccessRights rights) :
            base(string.Format("No such rights: {0}", rights.ToString()))
        {

        }
    }
    public class CaptchaRequiredException : Exception
    {
        public string Sid { get; private set; }
        public Uri Uri { get; private set; }

        public CaptchaRequiredException(string sid, Uri uri) : 
            base("Captcha required!")
        {
            Sid = sid;
            Uri = uri;
        }
    }
    public class NoMoreAttemptsException : Exception { }
}
