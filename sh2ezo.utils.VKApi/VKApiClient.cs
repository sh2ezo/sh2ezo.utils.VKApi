﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using sh2ezo.utils.VKApi.Models;
using sh2ezo.utils.VKApi.Helpers;
using System.Threading;

namespace sh2ezo.utils.VKApi
{
    public class VKApiClient
    {
        private HttpClient _client;
        private RequestBuilder _builder;
        private ICaptchaSolver _solver;
        private static Random _random = new Random();
        private static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public VKApiClient(HttpClient client, RequestBuilder builder, ICaptchaSolver solver)
        {
            _client = client;
            _builder = builder;
            _solver = solver;
        }

        public async Task<JToken> QueryAsync(string method, string accessToken, IDictionary<string, string> args)
        {
            const int MaxAttempts = 10;
            int attempts = MaxAttempts + 1;

            while(--attempts > 0)
            {
                var query = $"https://api.vk.com/method/{method}?access_token={accessToken}&v={Constants.Version}";
                var request = _builder.Build("POST", query);
                request.Content = new FormUrlEncodedContent(args);
                var response = await _client.SendAsync(request).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var json = JToken.Parse(responseString);

                if (JsonHelper.IsError(json))
                {
                    var error = json["error"].ToObject<ErrorResponse>();

                    if (error.Code == ErrorCode.InvalidAppToken || error.Code == ErrorCode.InvalidGroupToken)
                    {
                        throw new TokenExpiredException();
                    }
                    else if (error.Code == ErrorCode.CaptchaRequired)
                    {
                        if(attempts == 0 || _solver == null)
                        {
                            throw new CaptchaRequiredException(error.CaptchaSid, error.CaptchaImg);
                        }
                        else
                        {
                            if(attempts < MaxAttempts)
                            {
                                await _solver.WrongCaptcha().ConfigureAwait(false);
                            }

                            var captcha = await _solver.Solve(error.CaptchaImg).ConfigureAwait(false);

                            args["captcha_sid"] = error.CaptchaSid;
                            args["captcha_key"] = captcha;
                        }
                    }
                    else if (error.Code == ErrorCode.TooManyRequests)
                    {
                        int delay;
                        await _semaphore.WaitAsync();

                        try
                        {
                            delay = _random.Next(2000, 60000);
                        }
                        finally
                        {
                            _semaphore.Release();
                        }

                        await Task.Delay(delay).ConfigureAwait(false);
                    }
                    else
                    {
                        throw new Exception(error.Description);
                    }
                }
                else
                {
                    return json;
                }
            }

            throw new NoMoreAttemptsException();
        }
    }
}
