﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi.Requests
{
    public class UtilsGetLinkStatsRequest
    {
        public string key;
        public string accessKey;
        public string interval;
        public int? intervalsCount;
        public bool? extended;
    }
}
