﻿namespace sh2ezo.utils.VKApi.Requests
{
    public class MessagesGetDialogsRequest
    {
        public int Offset;
        public int? Count;
        public ulong? StartMessageId;
        public int? PreviewLength;
        public bool? Unread;
        public bool? Important;
        public bool? Unanswered;
    }
}
