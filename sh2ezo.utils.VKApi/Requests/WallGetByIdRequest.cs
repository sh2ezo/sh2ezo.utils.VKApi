﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi.Requests
{
    public class WallGetByIdRequest
    {
        public string[] posts;
        public bool? extended;
        public int? copyHistoruyDepth;
        public string[] fields;
    }
}
