﻿using sh2ezo.utils.VKApi.Models;

namespace sh2ezo.utils.VKApi.Requests
{
    public class WallCreateCommentRequest
    {
        public long? OwnerID;
        public ulong PostID;
        public ulong? FromGroup;
        public string Message;
        public long? ReplyToComment;
        public string[] Attachments;
        public ulong? StickerID;
        public string Guid;
    }
}
