﻿using sh2ezo.utils.VKApi.Models;

namespace sh2ezo.utils.VKApi.Requests
{
    public class MessagesSendRequest
    {
        public long? UserId;
        public long? RandomId;
        public long? PeerId;
        public string Domain;
        public long? ChatId;
        public long[] UserIds;
        public string Message;
        public float? Lat;
        public float? Long;
        public string[] Attachments;
        public long[] ForwardMessages;
        public ulong? StickerId;
    }
}
