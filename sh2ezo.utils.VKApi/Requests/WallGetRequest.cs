﻿using sh2ezo.utils.VKApi.Models.Filters;

namespace sh2ezo.utils.VKApi.Requests
{
    public class WallGetRequest
    {
        public long? OwnerID;
        public string Domain;
        public long? Offset;
        public long? Count;
        public WallPostsFilter? Filter;
        public bool? Extended;
        public string[] Fields;
    }
}
