﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi.Requests
{
    public class GroupsGetByIdRequest
    {
        public string[] groupIds;
        public string groupId;
        public string[] fields;
    }
}
