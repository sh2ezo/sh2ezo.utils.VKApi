﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi
{
    public class ResultInfo
    {
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }
    }
}
