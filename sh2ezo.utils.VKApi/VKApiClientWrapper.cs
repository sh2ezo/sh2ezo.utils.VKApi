﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using sh2ezo.utils.VKApi.Models;
using sh2ezo.utils.VKApi.Requests;
using sh2ezo.utils.VKApi.Helpers;

namespace sh2ezo.utils.VKApi
{
    public class VKApiClientWrapper
    {
        VKApiClient _client;
        AuthDataStorage _storage;
        long _userId;

        public VKApiClientWrapper(VKApiClient client, AuthDataStorage storage, long userId)
        {
            _client = client;
            _storage = storage;
            _userId = userId;
        }

        public async Task<Result<WallPosts>> WallGetAsync(WallGetRequest request)
        {
            try
            {
                var accessToken = _storage.GetAccessToken(_userId);
                var args = new Dictionary<string, string>();

                if (request.OwnerID.HasValue) args["owner_id"] = request.OwnerID.ToString();
                if (!string.IsNullOrEmpty(request.Domain)) args["domain"] = request.Domain;
                if (request.Offset.HasValue) args["offset"] = request.Offset.ToString();
                if (request.Count.HasValue) args["count"] = request.Count.ToString();
                if (request.Filter.HasValue) args["filter"] = EnumHelper.GetTextValue(request.Filter);
                if (request.Extended.HasValue) args["extended"] = request.Extended.ToString();
                if (request.Fields != null && request.Fields.Length > 0) args["fields"] = string.Join(",", request.Fields);

                var result = await _client.QueryAsync("wall.get", accessToken, args).ConfigureAwait(false);

                if(JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<WallPosts>.Fail(error, null);
                }

                return Result<WallPosts>.Success(result["response"].ToObject<WallPosts>());
            }
            catch (Exception ex)
            {
                return Result<WallPosts>.Fail(ex, null);
            }
        }

        public async Task<Result<long>> WallCreateCommentAsync(WallCreateCommentRequest request)
        {
            try
            {
                var token = _storage.GetAccessToken(_userId);
                var args = new Dictionary<string, string>();
                if (request.OwnerID.HasValue) args["owner_id"] = request.OwnerID.ToString();
                args["post_id"] = request.PostID.ToString();
                if (request.FromGroup.HasValue) args["from_group"] = request.FromGroup.ToString();
                if (request.Message != null) args["message"] = request.Message;
                if (request.ReplyToComment.HasValue) args["reply_to_comment"] = request.ReplyToComment.ToString();
                if (request.Attachments != null && request.Attachments.Length > 0) args["attachments"] = string.Join(",", request.Attachments);
                if (request.StickerID.HasValue) args["sticker_id"] = request.StickerID.ToString();
                args["guid"] = request.Guid;
                var result = await _client.QueryAsync("wall.createComment", token, args).ConfigureAwait(false);                

                if(JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<long>.Fail(error, 0);
                }

                return Result<long>.Success((long)result["response"]["comment_id"]);

            }catch(Exception ex)
            {
                return Result<long>.Fail(ex, 0);
            }
        }

        public async Task<Result<ShortLinkStats>> UtilsGetLinkStatsAsync(UtilsGetLinkStatsRequest request, long? ownerId)
        {
            try
            {
                if (!ownerId.HasValue)
                {
                    ownerId = _userId;
                }

                var token = _storage.GetAccessToken(ownerId.Value);
                var args = new Dictionary<string, string>();
                args["key"] = request.key;
                if(!string.IsNullOrEmpty(request.accessKey)) args["access_key"] = request.accessKey;
                if (!string.IsNullOrEmpty(request.interval)) args["interval"] = request.interval;
                if (request.intervalsCount.HasValue) args["intervals_count"] = request.intervalsCount.Value.ToString();
                if (request.extended.HasValue) args["extended"] = request.extended.Value ? "1" : "0";
                var result = await _client.QueryAsync("utils.getLinkStats", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<ShortLinkStats>.Fail(error, null);
                }

                if (result["response"].GetType() == typeof(JObject) && result["response"].HasValues)
                {
                    return Result<ShortLinkStats>.Success(result["response"].ToObject<ShortLinkStats>());
                }
                else
                {
                    return Result<ShortLinkStats>.Success(null);
                }
            }
            catch (Exception ex)
            {
                return Result<ShortLinkStats>.Fail(ex, null);
            }
        }

        public async Task<Result<ShortLinkInfo>> UtilsGetShortLinkAsync(string url, bool? isPrivate, long? ownerId)
        {
            try
            {
                if (!ownerId.HasValue)
                {
                    ownerId = _userId;
                }

                var token = _storage.GetAccessToken(ownerId.Value);
                var args = new Dictionary<string, string>();
                args["url"] = url;
                if (isPrivate.HasValue) args["private"] = isPrivate.Value ? "1" : "0";
                var result = await _client.QueryAsync("utils.getShortLink", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<ShortLinkInfo>.Fail(error, null);
                }
                else
                {
                    return Result<ShortLinkInfo>.Success(result["response"].ToObject<ShortLinkInfo>());
                }
            }catch(Exception ex)
            {
                return Result<ShortLinkInfo>.Fail(ex, null);
            }
        }

        public async Task<Result<WallPost[]>> WallGetByIdAsync(WallGetByIdRequest request)
        {
            try
            {
                var token = _storage.GetAccessToken(_userId);
                var args = new Dictionary<string, string>();
                if (request.posts != null && request.posts.Length > 0) args["posts"] = string.Join(",", request.posts);
                if (request.extended.HasValue) args["extended"] = request.extended.Value ? "1" : "0";
                if (request.copyHistoruyDepth.HasValue) args["copy_history_depth"] = request.copyHistoruyDepth.ToString();
                if (request.fields != null && request.fields.Length > 0) args["fields"] = string.Join(",", request.fields);
                var result = await _client.QueryAsync("wall.getById", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<WallPost[]>.Fail(error, null);
                }
                else
                {
                    return Result<WallPost[]>.Success(result["response"].ToObject<WallPost[]>());
                }
            }
            catch (Exception ex)
            {
                return Result<WallPost[]>.Fail(ex, null);
            }
        }

        public async Task<Result<Group[]>> GroupsGetByIdAsync(GroupsGetByIdRequest request, long? ownerId)
        {
            try
            {
                if (!ownerId.HasValue)
                {
                    ownerId = _userId;
                }

                var token = _storage.GetAccessToken(ownerId.Value);
                var args = new Dictionary<string, string>();
                if (request.fields != null && request.fields.Length > 0) args["fields"] = string.Join(",", request.fields);
                if (!string.IsNullOrEmpty(request.groupId)) args["group_id"] = request.groupId;
                if (request.groupIds != null && request.groupIds.Length > 0) args["group_ids"] = string.Join(",", request.groupIds);
                var result = await _client.QueryAsync("groups.getById", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<Group[]>.Fail(error, null);
                }
                else
                {
                    return Result<Group[]>.Success(result["response"].ToObject<Group[]>());
                }
            }
            catch (Exception ex)
            {
                return Result<Group[]>.Fail(ex, null);
            }
        }

        public async Task<Result<Dialogs>> MessagesGetDialogsAsync(MessagesGetDialogsRequest request, long? ownerId)
        {
            try
            {
                if (!ownerId.HasValue)
                {
                    ownerId = _userId;
                }

                var token = _storage.GetAccessToken(ownerId.Value);
                var args = new Dictionary<string, string>();
                args["offset"] = request.Offset.ToString();
                if (request.Count.HasValue) args["count"] = request.Count.Value.ToString();
                if (request.StartMessageId.HasValue) args["start_message_id"] = request.StartMessageId.Value.ToString();
                if (request.PreviewLength.HasValue) args["preview_length"] = request.PreviewLength.Value.ToString();
                if (request.Unread.HasValue) args["unread"] = request.Unread.Value ? "1" : "0";
                if (request.Important.HasValue) args["important"] = request.Important.Value ? "1" : "0";
                if (request.Unanswered.HasValue) args["unanswered"] = request.Unanswered.Value ? "1" : "0";
                var result = await _client.QueryAsync("messages.getDialogs", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<Dialogs>.Fail(error, null);
                }
                else
                {
                    return Result<Dialogs>.Success(result["response"].ToObject<Dialogs>());
                }
            }
            catch (Exception ex)
            {
                return Result<Dialogs>.Fail(ex, null);
            }
        }

        public async Task<Result<bool>> MessagesSendAsync(MessagesSendRequest request, long? ownerId)
        {
            try
            {
                if (!ownerId.HasValue)
                {
                    ownerId = _userId;
                }

                var token = _storage.GetAccessToken(ownerId.Value);
                var args = new Dictionary<string, string>();
                if (request.UserId.HasValue) args["user_id"] = request.UserId.ToString();
                if (request.RandomId.HasValue) args["random_id"] = request.RandomId.ToString();
                if (request.PeerId.HasValue) args["peer_id"] = request.PeerId.ToString();
                if (!string.IsNullOrEmpty(request.Domain)) args["domain"] = request.Domain;
                if (request.ChatId.HasValue) args["chat_id"] = request.ChatId.ToString();
                if (request.UserIds != null && request.UserIds.Length > 0) args["user_ids"] = string.Join(",", request.UserIds);
                if (!string.IsNullOrEmpty(request.Message)) args["message"] = request.Message;
                if (request.Lat.HasValue) args["lat"] = request.Lat.ToString();
                if (request.Long.HasValue) args["long"] = request.Long.ToString();
                if (request.Attachments != null && request.Attachments.Length > 0) args["attachments"] = string.Join(",", request.Attachments);
                if (request.ForwardMessages != null && request.ForwardMessages.Length > 0) args["forward_messages"] = string.Join(",", request.ForwardMessages);
                if (request.StickerId.HasValue) args["sticker_id"] = request.StickerId.ToString();
                var result = await _client.QueryAsync("messages.send", token, args).ConfigureAwait(false);

                if (JsonHelper.IsError(result))
                {
                    var error = result["error"].ToObject<ErrorResponse>();
                    return Result<bool>.Fail(error, false);
                }
                else
                {
                    return Result<bool>.Success(true);
                }
            }
            catch (Exception ex)
            {
                return Result<bool>.Fail(ex, false);
            }
        }
    }
}
