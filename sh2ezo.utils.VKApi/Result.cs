﻿using System;
using sh2ezo.utils.VKApi.Models;

namespace sh2ezo.utils.VKApi
{
    public class Result<T>
    {
        public T Value { get; set; }
        public bool Succeded { get; set; }
        public ResultInfo Info { get; set; }

        public static Result<T> Fail(ErrorResponse error, T value)
        {
            var info = new ResultInfo
            {
                ErrorCode = (int)error.Code,
                Message = error.Description
            };

            return new Result<T>
            {
                Info = info,
                Succeded = false,
                Value = value
            };
        }

        public static Result<T> Fail(Exception exc, T value)
        {
            var info = new ResultInfo
            {
                Exception = exc,
                Message = exc.Message
            };

            return new Result<T>
            {
                Info = info,
                Succeded = false,
                Value = value
            };
        }

        public static Result<T> Success(T value)
        {
            return new Result<T>
            {
                Succeded = true,
                Value = value
            };
        }

        internal static Result<WallPosts> Fail(ErrorResponse error, object p)
        {
            throw new NotImplementedException();
        }
    }
}
