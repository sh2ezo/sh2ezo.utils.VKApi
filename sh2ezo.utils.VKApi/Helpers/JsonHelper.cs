﻿using Newtonsoft.Json.Linq;

namespace sh2ezo.utils.VKApi.Helpers
{
    internal static class JsonHelper
    {
        public static bool IsError(JToken json)
        {
            return json != null && json["error"] != null;
        }
    }
}
