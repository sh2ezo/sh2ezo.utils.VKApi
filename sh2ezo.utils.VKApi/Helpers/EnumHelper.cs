﻿using System;
using System.Reflection;
using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Helpers
{
    static class EnumHelper
    {
        public static string GetTextValue(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(EnumTextValueAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((EnumTextValueAttribute)attrs[0]).Value;
                }
            }

            return en.ToString();
        }
    }
}
