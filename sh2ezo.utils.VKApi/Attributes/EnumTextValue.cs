﻿using System;

namespace sh2ezo.utils.VKApi.Attributes
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    class EnumTextValueAttribute : Attribute
    {
        readonly string value;

        public EnumTextValueAttribute(string value)
        {
            this.value = value;
        }

        public string Value => value;
    }
}
