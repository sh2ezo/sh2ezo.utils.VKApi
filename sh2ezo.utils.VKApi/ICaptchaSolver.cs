﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi
{
    public interface ICaptchaSolver
    {
        Task<string> Solve(Uri uri);

        /// <summary>
        /// Последняя капча неверна
        /// </summary>
        Task WrongCaptcha();
    }
}
