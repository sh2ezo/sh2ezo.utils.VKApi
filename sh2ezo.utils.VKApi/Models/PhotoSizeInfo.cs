﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class PhotoSizeInfo
    {
        [JsonProperty("src")]
        public Uri Src;
        [JsonProperty("width")]
        public int? Width;
        [JsonProperty("height")]
        public int? Height;
        [JsonProperty("type")]
        public string Type;
    }
}
