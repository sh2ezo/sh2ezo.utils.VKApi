﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class CommentsInfo
    {
        [JsonProperty("count")]
        public int? Count;
        [JsonProperty("can_post")]
        public bool? CanPost;
    }
}
