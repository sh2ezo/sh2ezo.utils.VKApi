﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class PostSourceInfo
    {
        [JsonProperty("type")]
        public string Type;
        [JsonProperty("platform")]
        public string Platform;
        [JsonProperty("data")]
        public string Data;
        [JsonProperty("url")]
        public Uri Uri;
    }
}
