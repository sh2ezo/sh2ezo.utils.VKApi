﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Dialog
    {
        [JsonProperty("unread")]
        public int? Unread;
        [JsonProperty("in_read")]
        public long? InRead;
        [JsonProperty("out_read")]
        public long? OutRead;
        [JsonProperty("message")]
        public Message Message;
    }
}
