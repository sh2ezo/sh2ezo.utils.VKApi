﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Group
    {
        [JsonProperty("id")]
        public long ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("screen_name")]
        public string ScreenName { get; set; }
        [JsonProperty("is_closed")]
        public bool? IsClosed { get; set; }
        [JsonProperty("deactivated")]
        public string Deactivated { get; set; }
        [JsonProperty("is_admin")]
        public bool? IsAdmin { get; set; }
        [JsonProperty("admin_level")]
        public int? AdminLevel { get; set; }
        [JsonProperty("is_member")]
        public bool? IsMember { get; set; }
        [JsonProperty("invited_by")]
        public long? InvitedBy { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("photo_50")]
        public string Photo50 { get; set; }
        [JsonProperty("photo_100")]
        public string Photo100 { get; set; }
        [JsonProperty("photo_200")]
        public string Photo200 { get; set; }
        [JsonProperty("activity")]
        public string Activity { get; set; }
        [JsonProperty("age_limits")]
        public int? AgeLimitis { get; set; }
        [JsonProperty("ban_info")]
        public BanInfo BanInfo { get; set; }
        [JsonProperty("can_create_topic")]
        public bool? CanCreateTopic { get; set; }
        [JsonProperty("can_message")]
        public bool? CanMessage { get; set; }
        [JsonProperty("can_post")]
        public bool? CanPost { get; set; }
        [JsonProperty("can_see_all_posts")]
        public bool? CanSeeAllPosts { get; set; }
        [JsonProperty("can_upload_doc")]
        public bool? CanUploadDoc { get; set; }
        [JsonProperty("can_upload_video")]
        public bool? CanUploadVideo { get; set; }
        [JsonProperty("city")]
        public CityInfo City { get; set; }
        [JsonProperty("contacts")]
        public Contact[] Contacts { get; set; }
        [JsonProperty("counters")]
        public Counters Counters { get; set; }
        [JsonProperty("country")]
        public CountryInfo Country { get; set; }
        [JsonProperty("cover")]
        public Cover Cover { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("fixed_post")]
        public long? FixedPost { get; set; }
        [JsonProperty("has_photo")]
        public bool? HasPhoto { get; set; }
        [JsonProperty("is_favorite")]
        public bool? IsFavorite { get; set; }
        [JsonProperty("is_hidden_from_feed")]
        public bool? IsHiddenFromFeed { get; set; }
        [JsonProperty("is_messages_blocked")]
        public bool? IsMessagesBlocked { get; set; }
        [JsonProperty("links")]
        public GroupLink[] Links { get; set; }
        [JsonProperty("main_album_id")]
        public long? MainAlbumId { get; set; }
        [JsonProperty("main_section")]
        public int? MainSection { get; set; }
        [JsonProperty("member_status")]
        public int? MainStatus { get; set; }
        [JsonProperty("members_count")]
        public int? MembersCount { get; set; }
        [JsonProperty("place")]
        public PlaceInfo Place { get; set; }
        [JsonProperty("public_date_label")]
        public string PublicDateLabel { get; set; }
        [JsonProperty("site")]
        public string Site { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("verified")]
        public bool? Verified { get; set; }
        [JsonProperty("wiki_page")]
        public string WikiPage { get; set; }
    }
}
