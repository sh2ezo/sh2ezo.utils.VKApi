﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace sh2ezo.utils.VKApi.Models
{
    public class Message
    {
        [JsonProperty("id")]
        public long? Id;
        [JsonProperty("user_id")]
        public long? UserId;
        [JsonProperty("from_id")]
        public long? FromId;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Date;
        [JsonProperty("read_state")]
        public bool? ReadState;
        [JsonProperty("out")]
        public MessageType? MessageType;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("body")]
        public string Body;
        [JsonProperty("geo")]
        public Geo Geo;
        [JsonProperty("attachments")]
        AttachmentInfo[] Attachments;
        [JsonProperty("fwd_messages")]
        Message[] FwdMessages;
        [JsonProperty("emoji")]
        public bool Emoji;
        [JsonProperty("important")]
        public bool Important;
        [JsonProperty("deleted")]
        public bool? Deleted;
        [JsonProperty("random_id")]
        public long? RandomId;
        [JsonProperty("chat_id")]
        public long? ChatId;
        [JsonProperty("chat_active")]
        public long[] ChatActive;
        [JsonProperty("push_settings")]
        public object PushSettings;
        [JsonProperty("users_count")]
        public long? UsersCount;
        [JsonProperty("admin_id")]
        public long? AdminId;
        [JsonProperty("action")]
        public string Action;
        [JsonProperty("action_mid")]
        public long? ActionMid;
        [JsonProperty("action_email")]
        public string ActionEmail;
        [JsonProperty("action_text")]
        public string ActionText;
        [JsonProperty("photo_50")]
        public string Photo50;
        [JsonProperty("photo_100")]
        public string Photo100;
        [JsonProperty("photo_200")]
        public string Photo200;
    }
}
