﻿using Newtonsoft.Json;
using sh2ezo.utils.VKApi.Models.Attachments;

namespace sh2ezo.utils.VKApi.Models
{
    public class AttachmentInfo
    {
        [JsonProperty("type")]
        public string Type;
        [JsonProperty("photo")]
        private Photo Photo;
        [JsonProperty("posted_photo")]
        private PostedPhoto PostedPhoto;
        [JsonProperty("video")]
        private Video Video;
        [JsonProperty("audio")]
        private Audio Audio;
        [JsonProperty("doc")]
        private Document Document;
        [JsonProperty("graffiti")]
        private Graffiti Graffiti;
        [JsonProperty("link")]
        private Link Link;
        [JsonProperty("note")]
        private Note Note;
        [JsonProperty("app")]
        private App App;
        [JsonProperty("poll")]
        private Poll Poll;
        [JsonProperty("page")]
        private WikiPage Page;
        [JsonProperty("album")]
        private Album Album;
        [JsonProperty("photos_list")]
        private string[] PhotosList;
        [JsonProperty("market")]
        private Market Market;
        [JsonProperty("market_album")]
        private MarketAlbum MarketAlbum;
        [JsonProperty("sticker")]
        private Sticker Sticker;

        public object Attachment
        {
            get
            {
                switch (Type)
                {
                    case "photo":
                        return Photo;
                    case "posted_photo":
                        return PostedPhoto;
                    case "video":
                        return Video;
                    case "audio":
                        return Audio;
                    case "doc":
                        return Document;
                    case "graffiti":
                        return Graffiti;
                    case "link":
                        return Link;
                    case "app":
                        return App;
                    case "poll":
                        return Poll;
                    case "page":
                        return Page;
                    case "album":
                        return Album;
                    case "photos_list":
                        return PhotosList;
                    case "market":
                        return Market;
                    case "market_album":
                        return MarketAlbum;
                    case "sticker":
                        return Sticker;
                    default:
                        return null;
                }
            }
        }
    }
}
