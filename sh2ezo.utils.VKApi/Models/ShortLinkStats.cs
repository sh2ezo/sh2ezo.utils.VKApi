﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ShortLinkStats
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("stats")]
        public ShortLinkTimestampStats[] Stats { get; set; }

    }
}
