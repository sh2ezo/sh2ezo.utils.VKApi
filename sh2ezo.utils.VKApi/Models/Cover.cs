﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Cover
    {
        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }
        [JsonProperty("images")]
        public CoverImage[] Images { get; set; }
    }
}
