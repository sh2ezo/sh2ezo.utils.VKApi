﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ShortLinkTimestampStats
    {
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        [JsonProperty("views")]
        public long Views { get; set; }
        [JsonProperty("sex_age")]
        public ShortLinkSexAgeStats[] SexAge { get; set; }
        [JsonProperty("countries")]
        public ShortLinkCountryStats[] Countries { get; set; }
        [JsonProperty("cities")]
        public ShortLinkCityStats[] Cities { get; set; }
    }
}
