﻿using Newtonsoft.Json;
using System;

namespace sh2ezo.utils.VKApi.Models
{
    public class ErrorResponse
    {
        [JsonProperty("error_code")]
        public ErrorCode Code { get; set; }
        [JsonProperty("error_msg")]
        public string Description { get; set; }
        [JsonProperty("captcha_sid")]
        public string CaptchaSid { get; set; }
        [JsonProperty("captcha_img")]
        public Uri CaptchaImg { get; set; }
    }
}
