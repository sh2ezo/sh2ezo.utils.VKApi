﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Note
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("text")]
        public string Text;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("comments")]
        public long? Comments;
        /// <summary>
        /// Количество прочитанных комментариев (только при запросе информации о заметке текущего пользователя).
        /// </summary>
        [JsonProperty("read_comments")]
        public long? ReadComments;
        [JsonProperty("view_url")]
        public Uri ViewUri;
    }
}
