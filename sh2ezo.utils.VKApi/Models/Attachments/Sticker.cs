﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Sticker
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("product_id")]
        public long? ProductID;
        [JsonProperty("photo_64")]
        public Uri Photo64;
        [JsonProperty("photo_128")]
        public Uri Photo128;
        [JsonProperty("photo_256")]
        public Uri Photo256;
        [JsonProperty("photo_352")]
        public Uri Photo352;
        [JsonProperty("width")]
        public int? Width;
        [JsonProperty("height")]
        public int? Height;
    }
}
