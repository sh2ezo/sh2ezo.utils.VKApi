﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class MarketAlbum
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("photo")]
        public Photo Photo;
        [JsonProperty("count")]
        public int? Count;
        [JsonProperty("updated_time")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? UpdatedTime;
    }
}
