﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using sh2ezo.utils.VKApi.Models.Attachments.Preview;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Document
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("size")]
        public long? Size;
        [JsonProperty("ext")]
        public string Extension;
        [JsonProperty("url")]
        public Uri Uri;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("type")]
        public DocumentType Type;
        [JsonProperty("preview")]
        public PreviewInfo Preview;
    }
}
