﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Photo
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("album_id")]
        public long? AlbumID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("user_id")]
        public long? UserID;
        [JsonProperty("text")]
        public string Text;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Date;
        [JsonProperty("sizes")]
        public PhotoSizeInfo[] Sizes;
        [JsonProperty("photo_75")]
        public Uri Photo75;
        [JsonProperty("photo_130")]
        public Uri Photo130;
        [JsonProperty("photo_604")]
        public Uri Photo604;
        [JsonProperty("photo_807")]
        public Uri Photo807;
        [JsonProperty("photo_1280")]
        public Uri Photo1280;
        [JsonProperty("photo_2560")]
        public Uri Photo2560;
        [JsonProperty("width")]
        public int? Width;
        [JsonProperty("height")]
        public int? Height;
    }
}
