﻿using Newtonsoft.Json;
using sh2ezo.utils.VKApi.Models.Attachments;

namespace sh2ezo.utils.VKApi.Models.Attachments.Preview
{
    public class PreviewInfo
    {
        [JsonProperty("photo")]
        public PhotoPreviewInfo Photo;
        [JsonProperty("graffiti")]
        public GraffitiPreviewInfo Graffiti;
        [JsonProperty("audio_msg")]
        public AudioMsgPreviewInfo AudioMsg;
    }
}
