﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments.Preview
{
    public class PhotoPreviewInfo
    {
        [JsonProperty("sizes")]
        PhotoSizeInfo[] Sizes { get; set; }
    }
}
