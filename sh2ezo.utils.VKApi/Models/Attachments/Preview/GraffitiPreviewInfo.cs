﻿using Newtonsoft.Json;
using System;

namespace sh2ezo.utils.VKApi.Models.Attachments.Preview
{
    public class GraffitiPreviewInfo
    {
        [JsonProperty("src")]
        public Uri Src;
        [JsonProperty("width")]
        public int? Width;
        [JsonProperty("height")]
        public int? Height;
    }
}
