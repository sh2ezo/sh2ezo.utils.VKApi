﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments.Preview
{
    public class AudioMsgPreviewInfo
    {
        [JsonProperty("duration")]
        public long? Duration;
        [JsonProperty("waveform")]
        public int[] Waveform;
        [JsonProperty("link_ogg")]
        public Uri LinkOGG;
        [JsonProperty("link_mp3")]
        public Uri LinkMP3;
    }
}
