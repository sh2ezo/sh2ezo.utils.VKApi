﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Audio
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("artist")]
        public string Artist;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("duration")]
        public long? Duration;
        [JsonProperty("url")]
        public Uri Uri;
        [JsonProperty("lyrics_id")]
        public long? LyricsID;
        [JsonProperty("album_id")]
        public long? AlbumID;
        [JsonProperty("genre_id")]
        public long? GenreID;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("no_search")]
        public bool? NoSearch;
    }
}
