﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Album
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("thumb")]
        public Photo Thumb;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("description")]
        public string Description;
        [JsonProperty("created")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Created;
        [JsonProperty("updated")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Updated;
        [JsonProperty("size")]
        public int? Size;
    }
}
