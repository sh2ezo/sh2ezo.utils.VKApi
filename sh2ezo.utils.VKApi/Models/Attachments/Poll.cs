﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Poll
    {
        [JsonProperty("poll")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("created")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Created;
        [JsonProperty("question")]
        public string Question;
        [JsonProperty("votes")]
        public long? Votes;
        [JsonProperty("answer_id")]
        public long? AnswerID;
        [JsonProperty("answers")]
        public AnswerInfo[] Answers;
        [JsonProperty("Anonymous")]
        public bool? Anonymous;
    }
}
