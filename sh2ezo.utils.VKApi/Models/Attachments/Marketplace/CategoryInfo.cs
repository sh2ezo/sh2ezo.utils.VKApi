﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments.Marketplace
{
    public class CategoryInfo
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("section")]
        public SectionInfo Section;
    }
}
