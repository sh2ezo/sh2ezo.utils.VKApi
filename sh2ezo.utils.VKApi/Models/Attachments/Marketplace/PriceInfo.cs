﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments.Marketplace
{
    public class PriceInfo
    {
        /// <summary>
        /// Целочисленное значение цены, умноженное на 100
        /// </summary>
        [JsonProperty("amount")]
        public long? Amount;
        [JsonProperty("currency")]
        public CurrencyInfo Currency;
        [JsonProperty("text")]
        public string Text;
    }
}
