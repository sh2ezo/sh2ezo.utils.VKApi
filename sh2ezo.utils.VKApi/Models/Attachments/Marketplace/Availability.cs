﻿namespace sh2ezo.utils.VKApi.Models.Attachments.Marketplace
{
    public enum Availability
    {
        Available = 0,
        Removed = 1,
        NotAvailable = 2
    }
}
