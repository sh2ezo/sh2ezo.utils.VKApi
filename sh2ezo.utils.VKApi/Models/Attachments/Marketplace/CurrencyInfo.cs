﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments.Marketplace
{
    public class CurrencyInfo
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("name")]
        public string Name;
    }
}
