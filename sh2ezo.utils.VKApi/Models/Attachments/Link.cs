﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Link
    {
        [JsonProperty("url")]
        public Uri Uri;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("caption")]
        public string Caption;
        [JsonProperty("description")]
        public string Description;
        [JsonProperty("photo")]
        public Photo Photo;
        [JsonProperty("is_external")]
        public bool? IsExternal;
        [JsonProperty("product")]
        public ProductInfo Product;
        [JsonProperty("button")]
        public ButtonInfo Button;
        /// <summary>
        /// Идентификатор вики-страницы с контентом для предпросмотра содержимого страницы. Возвращается в формате "owner_id_page_id".
        /// </summary>
        [JsonProperty("preview_page")]
        public string PreviewPage;
        [JsonProperty("preview_url")]
        public Uri PreviewUri;
    }
}
