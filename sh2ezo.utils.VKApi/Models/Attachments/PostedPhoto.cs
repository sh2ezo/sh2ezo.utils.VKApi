﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class PostedPhoto
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("photo_130")]
        public Uri Photo130;
        [JsonProperty("photo_604")]
        public Uri Photo604;
    }
}
