﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class App
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("photo_130")]
        public Uri Photo130;
        [JsonProperty("photo_604")]
        public Uri Photo604;
    }
}
