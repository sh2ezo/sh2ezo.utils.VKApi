﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Video
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("description")]
        public string Description;
        [JsonProperty("duration")]
        public long? Duration;
        [JsonProperty("photo_130")]
        public string Photo130;
        [JsonProperty("photo_320")]
        public string Photo320;
        [JsonProperty("photo_640")]
        public string Photo640;
        [JsonProperty("photo_800")]
        public string Photo800;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("adding_date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? AddingDate;
        [JsonProperty("views")]
        public long? Views;
        [JsonProperty("comments")]
        public long? Comments;
        [JsonProperty("player")]
        public string Player;
        [JsonProperty("access_key")]
        public string AccessKey;
        [JsonProperty("processing")]
        public bool? Processing;
        [JsonProperty("live")]
        public bool? Live;
        [JsonProperty("upcoming")]
        public bool? Upcoming;
    }
}
