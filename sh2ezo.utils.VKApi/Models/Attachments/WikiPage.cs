﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class WikiPage
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("group_id")]
        public long? GroupID;
        [JsonProperty("creator_id")]
        public long? CreatorID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("current_user_can_edit")]
        public bool? CurrentUserCanEdit;
        [JsonProperty("current_user_can_edit_access")]
        public bool? CurrentUserCanEditAccess;
        [JsonProperty("who_can_view")]
        public ObjectAccessPolicy WhoCanView;
        [JsonProperty("who_can_edit")]
        public ObjectAccessPolicy WhoCanEdit;
        [JsonProperty("edited")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Edited;
        [JsonProperty("created")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Created;
        [JsonProperty("editor_id")]
        public long? EditorID;
        [JsonProperty("views")]
        public long? Views;
        /// <summary>
        /// Заголовок родительской страницы для навигации, если есть.
        /// </summary>
        [JsonProperty("parent")]
        public string Parent;
        /// <summary>
        /// Заголовок второй родительской страницы для навигации, если есть
        /// </summary>
        [JsonProperty("parent2")]
        public string Parent2;
        [JsonProperty("source")]
        public string Source;
        [JsonProperty("html")]
        public string Html;
        [JsonProperty("view_url")]
        public Uri ViewUri;
    }
}
