﻿using System;
using Newtonsoft.Json;
using sh2ezo.utils.VKApi.Models.Attachments.Marketplace;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models.Attachments
{
    public class Market
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("description")]
        public string Description;
        [JsonProperty("price")]
        public PriceInfo Price;
        [JsonProperty("category")]
        public CategoryInfo Category;
        [JsonProperty("thumb_photo")]
        public Uri ThumbPhoto;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("availability")]
        public Availability? Availability;
        [JsonProperty("photos")]
        public Photo[] Photos;
        [JsonProperty("can_comment")]
        public bool? CanComment;
        [JsonProperty("can_repost")]
        public bool? CanRepost;
        [JsonProperty("likes")]
        public LikesInfo Likes;
    }
}
