﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models.Filters
{
    public enum WallPostsFilter
    {
        [EnumTextValue("suggests")]
        Suggests,
        [EnumTextValue("postponed")]
        Postponed,
        [EnumTextValue("owner")]
        Owner,
        [EnumTextValue("others")]
        Others,
        [EnumTextValue("all")]
        All
    }
}
