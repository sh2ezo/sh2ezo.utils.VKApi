﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ShortLinkCityStats
    {
        [JsonProperty("city_id")]
        public string CityId { get; set; }
        [JsonProperty("views")]
        public int Views { get; set; }
    }
}
