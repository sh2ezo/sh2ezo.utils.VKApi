﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class WallPosts
    {
        [JsonProperty("count")]
        public long? Count;
        [JsonProperty("items")]
        public WallPost[] Items;
    }
}
