﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ViewsInfo
    {
        [JsonProperty("count")]
        public int? Count;
    }
}
