﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models
{
    public enum DocumentType
    {
        Text = 1,
        Archive = 2,
        GIF = 3,
        Picture = 4,
        Audio = 5,
        Video = 6,
        eBook = 7,
        Unknown = 8
    }
}
