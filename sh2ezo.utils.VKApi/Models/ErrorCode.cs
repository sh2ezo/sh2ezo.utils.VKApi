﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.VKApi.Models
{
    public enum ErrorCode
    {
        UnknownError = 1,
        ApplicationDisabled = 2,
        UnknownMethod = 3,
        InvalidSign = 4,
        AuthFailed = 5,
        TooManyRequests = 6,
        NoRights = 7,
        WrongRequest = 8,
        FloodDetected = 9,
        InternalError = 10,
        TestMode = 11,
        CaptchaRequired = 14,
        AccessDenied = 15,
        HTTPSRequired = 16,
        ValidationRequired = 17,
        AccountRemovedOrBanned = 18,
        StandaloneRequired = 20,
        StandaloneOrOpenAPIRequired = 21,
        MethodDisabled = 23,
        ValidationFromUserRequired = 24,
        InvalidAppToken = 27,
        InvalidGroupToken = 28,
        InvalidParameters = 100,
        InvalidApiID = 101,
        InvalidUserID = 113,
        InvalidTimestamp = 150,
        AlbumAccessDenied = 200,
        AudioAccessDenied = 201,
        GroupAccessDenied = 203,
        AlbumOverflow = 300,
        PayRequired = 500,
        NoRightsForAdsManagement = 600,
        AdsManagementError = 603
    }
}
