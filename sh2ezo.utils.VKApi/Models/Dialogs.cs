﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Dialogs
    {
        [JsonProperty("count")]
        public int Count;
        [JsonProperty("unread_dialogs")]
        public int? UnreadDialogs;
        [JsonProperty("items")]
        public Dialog[] Items;
    }
}
