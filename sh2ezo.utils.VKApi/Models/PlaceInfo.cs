﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models
{
    public class PlaceInfo
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("latitude")]
        public double Latitude;
        [JsonProperty("longitude")]
        public double Longitude;
        [JsonProperty("created")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Created;
        [JsonProperty("icon")]
        public Uri Icon;
        [JsonProperty("country")]
        public string Country;
        [JsonProperty("city")]
        public string City;
        [JsonProperty("type")]
        public int? Type;
        [JsonProperty("group_id")]
        public long? GroupID;
        [JsonProperty("group_photo")]
        public Uri GroupPhoto;
        [JsonProperty("checkins")]
        public long? Checkins;
        [JsonProperty("updated")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Updated;
        [JsonProperty("address")]
        public long? Address;
    }
}
