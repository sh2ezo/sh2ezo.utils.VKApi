﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class RepostsInfo
    {
        [JsonProperty("count")]
        public int? Count;
        [JsonProperty("user_reposted")]
        public bool? UserReposted;
    }
}
