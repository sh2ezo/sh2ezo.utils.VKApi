﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Counters
    {
        [JsonProperty("photos")]
        public int? Photos { get; set; }
        [JsonProperty("albums")]
        public int? Albums { get; set; }
        [JsonProperty("audios")]
        public int? Audios { get; set; }
        [JsonProperty("videos")]
        public int? Videos { get; set; }
        [JsonProperty("topics")]
        public int? Topics { get; set; }
        [JsonProperty("docs")]
        public int? Docs { get; set; }
    }
}
