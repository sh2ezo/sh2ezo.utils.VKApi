﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sh2ezo.utils.VKApi.Models
{
    public class WallPost
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("owner_id")]
        public long? OwnerID;
        [JsonProperty("from_id")]
        public long? FromID;
        [JsonProperty("date")]
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime? Date;
        [JsonProperty("text")]
        public string Text;
        [JsonProperty("reply_owner_id")]
        public long? ReplyOwnerID;
        [JsonProperty("reply_post_id")]
        public long? ReplyPostID;
        [JsonProperty("friends_only")]
        public bool? FriendsOnly;
        [JsonProperty("comments")]
        public CommentsInfo Comments;
        [JsonProperty("likes")]
        public LikesInfo Likes;
        [JsonProperty("reposts")]
        public RepostsInfo Reposts;
        [JsonProperty("views")]
        public ViewsInfo Views;
        [JsonProperty("post_source")]
        public PostSourceInfo PostSource;
        [JsonProperty("attachments")]
        public AttachmentInfo[] Attachments;
        [JsonProperty("geo")]
        public Geo Geo;
        [JsonProperty("signer_id")]
        public long? SignerID;
        [JsonProperty("copy_history")]
        public WallPost[] CopyHistory;
        [JsonProperty("can_pin")]
        public bool? CanPin;
        [JsonProperty("can_delete")]
        public bool? CanDelete;
        [JsonProperty("can_edit")]
        public bool? CanEdit;
        [JsonProperty("is_pinned")]
        public bool? IsPinned;
        [JsonProperty("marked_as_ads")]
        public bool? MarkedAsAds;
    }
}
