﻿namespace sh2ezo.utils.VKApi.Models
{
    public enum ObjectAccessPolicy
    {
        All = 2,
        GroupMembers = 1,
        Managers = 0
    }
}
