﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class AnswerInfo
    {
        [JsonProperty("id")]
        public long? ID;
        [JsonProperty("text")]
        public string Text;
        [JsonProperty("votes")]
        public long? Votes;
        [JsonProperty("rate")]
        public double? Rate;
    }
}
