﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Geo
    {
        [JsonProperty("type")]
        public string Type;
        [JsonProperty("coordinates")]
        public string Coordinates;
        [JsonProperty("place")]
        public PlaceInfo Place;
    }
}
