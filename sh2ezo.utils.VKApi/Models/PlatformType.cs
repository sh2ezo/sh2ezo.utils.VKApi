﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models
{
    public enum PlatformType
    {
        [EnumTextValueAttribute("android")]
        Android,
        [EnumTextValueAttribute("iphone")]
        iPhone,
        [EnumTextValueAttribute("wphone")]
        WindowsPhone
    }
}
