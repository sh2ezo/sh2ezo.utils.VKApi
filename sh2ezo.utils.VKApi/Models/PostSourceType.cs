﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models
{
    public enum PostSourceType
    {
        [EnumTextValueAttribute("vk")]
        VK,
        [EnumTextValueAttribute("widget")]
        Widget,
        [EnumTextValueAttribute("api")]
        Api,
        [EnumTextValueAttribute("rss")]
        RSS,
        [EnumTextValueAttribute("sms")]
        SMS
    }
}
