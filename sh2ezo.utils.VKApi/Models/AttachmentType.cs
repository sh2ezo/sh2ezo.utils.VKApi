﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models
{
    public enum AttachmentType
    {
        [EnumTextValueAttribute("photo")]
        Photo,
        [EnumTextValueAttribute("posted_photo")]
        PostedPhoto,
        [EnumTextValueAttribute("video")]
        Video,
        [EnumTextValueAttribute("audio")]
        Audio,
        [EnumTextValueAttribute("doc")]
        Doc,
        [EnumTextValueAttribute("graffiti")]
        Graffiti,
        [EnumTextValueAttribute("link")]
        Link,
        [EnumTextValueAttribute("app")]
        App,
        [EnumTextValueAttribute("poll")]
        Poll,
        [EnumTextValueAttribute("page")]
        Page,
        [EnumTextValueAttribute("album")]
        Album,
        [EnumTextValueAttribute("photos_list")]
        PhotosList,
        [EnumTextValueAttribute("market")]
        Market,
        [EnumTextValueAttribute("market_album")]
        MarketAlbum,
        [EnumTextValueAttribute("sticker")]
        Sticker
    }
}
