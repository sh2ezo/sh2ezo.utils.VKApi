﻿using sh2ezo.utils.VKApi.Attributes;

namespace sh2ezo.utils.VKApi.Models
{
    public enum ButtonActionType
    {
        [EnumTextValueAttribute("open_url")]
        OpenUrl
    }
}
