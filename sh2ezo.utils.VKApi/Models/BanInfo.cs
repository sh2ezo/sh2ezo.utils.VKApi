﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class BanInfo
    {
        [JsonProperty("end_date")]
        public long EndDate { get; set; }
        [JsonProperty("comment")]
        public string Comment { get; set; }
    }
}
