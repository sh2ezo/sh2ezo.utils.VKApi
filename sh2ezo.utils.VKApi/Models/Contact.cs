﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class Contact
    {
        [JsonProperty("user_id")]
        public long? UserId { get; set; }
        [JsonProperty("desc")]
        public string Desc { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
