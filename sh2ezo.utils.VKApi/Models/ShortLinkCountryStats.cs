﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ShortLinkCountryStats
    {
        [JsonProperty("country_id")]
        public string CountryId { get; set; }
        [JsonProperty("views")]
        public int Views { get; set; }
    }
}
