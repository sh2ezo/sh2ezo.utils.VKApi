﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ShortLinkSexAgeStats
    {
        [JsonProperty("age_range")]
        public string AgeRange { get; set; }
        [JsonProperty("female")]
        public int Female { get; set; }
        [JsonProperty("male")]
        public int Male { get; set; }
    }
}
