﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class CityInfo
    {
        [JsonProperty("id")]
        public int? ID { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
