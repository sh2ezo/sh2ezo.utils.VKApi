﻿using System;
using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ButtonActionInfo
    {
        [JsonProperty("type")]
        public string Type;
        [JsonProperty("url")]
        public Uri Uri;
    }
}
