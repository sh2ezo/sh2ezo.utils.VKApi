﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class ButtonInfo
    {
        [JsonProperty("title")]
        public string Title;
        [JsonProperty("action")]
        public ButtonActionInfo Action;
    }
}
