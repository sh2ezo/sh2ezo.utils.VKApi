﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class LikesInfo
    {
        [JsonProperty("count")]
        public int? Count;
        [JsonProperty("user_likes")]
        public bool? UserLikes;
        [JsonProperty("can_like")]
        public bool? CanLike;
        [JsonProperty("can_publish")]
        public bool? CanPublish;
    }
}
