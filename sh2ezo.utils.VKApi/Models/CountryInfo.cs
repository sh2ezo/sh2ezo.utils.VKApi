﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class CountryInfo
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
