﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class GroupLink
    {
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("desc")]
        public string Desc { get; set; }
        [JsonProperty("photo_50")]
        public string Photo50 { get; set; }
        [JsonProperty("photo_100")]
        public string Photo100 { get; set; }
    }
}
