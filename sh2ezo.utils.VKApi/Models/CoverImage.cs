﻿using Newtonsoft.Json;

namespace sh2ezo.utils.VKApi.Models
{
    public class CoverImage
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("width")]
        public int? Width { get; set; }
        [JsonProperty("height")]
        public int? Height { get; set; }
    }
}
