﻿using sh2ezo.utils.VKApi.Models;
using System;

namespace sh2ezo.utils.VKApi
{
    public struct AppAuthData
    {
        public long SubjectID;
        public string AccessToken;
        public AccessRights Rights;
        public DateTime TokenExpireTime;
    }
}
