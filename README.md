# Description

It's my implementation of .NET client library for VKontakte API.

# Example of usage

```csharp

var client = new HttpClient(); // create http client
var requestBuilder = new RequestBuilder("My user agent"); // create request builder
var apiClient = new VKApiClient(client, requestBuilder, null); // create raw api client (it can call any method from VKApi)
var user = new UserData // some data about user and application
{
    AppID = 1, // your personal app id
    Username = "username", // username
    Password = "password" // password
};

var accessTokenSource = new AccessTokenSource(client, requestBuilder, user); // create source, which will be used to get an access tokens during application lifetime
var vkLogin = await accessTokenSource.LoginAsync().ConfigureAwait(false); // login to account

if(vkLogin.Succeded)
{
    var authData = await accessTokenSource.UserAuthAsync( // get access token with some rights
        AccessRights.Wall |
        AccessRights.Messages
    ).ConfigureAwait(false);

    if(authData.Succeded)
    {
        var authDataStorage = new AuthDataStorage(); // create storage for authentication data. it can be a singletone
        authDataStorage.AddOrUpdate(authData.Value.SubjectID, authData.Value); // store an access token gotten a moment ago
        authDataStorage.TokenExpired += (storage, subjectId) => // add event handler for the case our token will become an expired
        {
            // get access token for user (it can also be a group) again
            var newAuthData = accesstokenSource.UserAuthAsync(
                AccessRights.Wall |
                AccessRights.Messages
            ).Result;

            if(newAuthData.Succeded) // we have a new token
            {
                storage.AddOrUpdate(subjectId, newAuthData.Value);
                return true;
            }
            else // failure
            {
                return false;
            }
        }

        // create wrapper for raw client (raw client also can be used)
        var vk = new VKApiClientWrapper(apiClient, authDataStorage, authData.Value.SubjectID);

        // now we can send messages
        var sendMessage = await vk.MessagesSendAsync(new MessagesSendAsync
        {
            ChatId = 123456, // some chat number
            Messaage = "Hello, world!!!" // message text
        }, null).ConfigureAwait(false);

        if(!sendMessage.Succeded)
        {
            // handle failure
        }
    }
}

```